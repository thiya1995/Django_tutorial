## Full stack developer with HTML, CSS and Javascript in Tamil


**Course Name :** HTML, CSS and Javascript for Web Developer 

**Instructors :** Athithya

**Course Duration :** 40 days

**Course model :** Teaching all the modules for 1hour per day in a online meeting. Q&A sessions will be for 2 hours at the end of the week in a online meeting. Learning content worth 2 - 4 hours will be available for paid participants to view. In total, we will deliver ~7hours live sessions with the participants. Week Related topics Resources and Exercises will be delivered to the participants on 4th day of the week and participants should submit the exercise within 2days.

**Course Cost :** 1500 / person

**Promises :** Intership in Chiguru.tech for 1 person for a period of 3 months to work on 1 of our projects.

**Course level :** Beginner

**Introductory course : Introduction about the courses what they are going to learn during the courses - 1 hour (Live)**

##### Day1 - Elements and Structure ( 1 hour)

| Topic | Sub Topics | Instructor | Duration | Day of the week
| ------ | ------ | ------------- | ---------- |-------------|
| Elements and Structure | Introduction to HTML | Athithya | 10mins | 1 |
| Elements and Structure | HTML Anatomy | Athithya | 10mins | 1 |
| Elements and Structure | The body | Athithya | 5mins | 1 |
| Elements and Structure | Headings | Athithya | 5mins | 1 |
| Elements and Structure | Divs | Athithya | 5mins | 1 |
| Elements and Structure | Attributes | Athithya | 10mins | 1 |
| Elements and Structure | Headings | Athithya | 5mins | 1 |
| Elements and Structure | Displaying Text | Athithya | 5mins | 1 |
| Elements and Structure | Wrap up | Athithya | 5mins | 1 |


